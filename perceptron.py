import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import math

n_input,n_hidden_1,n_hidden_2,n_classes=1,10,5,1
learningRate=0.1

x = tf.placeholder("float", [None, n_input])
y = tf.placeholder("float", [None, n_classes])

weights = {
	'h1': tf.Variable(tf.random_uniform([n_input, n_hidden_1],-1.0,1.0)),
	'h2': tf.Variable(tf.random_uniform([n_hidden_1, n_hidden_2],-1.0,1.0)),
	'out': tf.Variable(tf.random_uniform([n_hidden_2, n_classes],-1.0,1.0))
}
biases = {
	'b1': tf.Variable(tf.random_uniform([n_hidden_1],-1.0,1.0)),
	'b2': tf.Variable(tf.random_uniform([n_hidden_2],-1.0,1.0)),
	'out': tf.Variable(tf.random_uniform([n_classes],-1.0,1.0))
}

def function(x):
	return 2*x**3-x

def generateSample(f,x):
	y=[]
	for i in x:
		y.append(f(i))
	return y

xSample=np.arange(-1,1.02,0.02)
ySample=generateSample(function,xSample)
xValidationSample=np.arange(-1,1.1,0.1)
yValidationSample=generateSample(function,xValidationSample)

def generateModel(x,w,b):
	layer1=tf.add(tf.matmul(x,w['h1']),b['b1'])
	layer1=tf.tanh(layer1)
	layer2=tf.add(tf.matmul(layer1,w['h2']),b['b2'])
	layer2=tf.tanh(layer2)
	outLayer=tf.add(tf.matmul(layer2,w['out']),b['out'])
	return outLayer

model = generateModel(x, weights, biases)
loss = tf.reduce_mean(tf.square(model - y))
optimizer = tf.train.GradientDescentOptimizer(learningRate)
train = optimizer.minimize(loss)

init = tf.global_variables_initializer()

with tf.Session() as sess:
	sess.run(init)
	for i in range(10000):
		sess.run(train, feed_dict={x:np.transpose([xSample]), y:np.transpose([ySample])})
		if i%1000==0:
			print(sess.run(loss, feed_dict={x:np.transpose([xSample]), y:np.transpose([ySample])}))
	yResult=sess.run(model,feed_dict={x:np.transpose([xSample])})
	plt.plot(xSample,ySample)
	plt.plot(xValidationSample,yValidationSample,'ro')
	plt.show()